import "./App.css";
import { Header } from "./components/header";
import { Card } from "./components/card";

function App() {
  return (
    <div className="meme-generator">
      <Header />
      <Card />
      
      {/* <Random/> */}
    </div>
  );
}

export default App;
