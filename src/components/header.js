import React from "react";

import "./header.css";

function Header(){
    return(
        <nav className="header">
            <img src="./face.png" alt="face" width={30} height={30} />
            <p> Meme Generator</p>
        </nav>
    )
}

export {Header}