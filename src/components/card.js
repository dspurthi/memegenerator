import React from "react";

import "./card.css";

function Card() {
  const [memes_data, setMemesData] = React.useState([]);
  const [url, setUrl] = React.useState("");

  React.useEffect(function () {
    fetch("https://api.imgflip.com/get_memes")
      .then((response) => response.json())
      .then((data) => {
        setMemesData(data.data.memes);
      })
      .catch((err) => console.error(err));
  }, []);

  // React.useEffect(() => {
  //   changeImage();
  // }, [props]);

  function changeImage() {
    if (memes_data.length > 0) {
      const randomNumber = Math.floor(Math.random() * memes_data.length);
      setUrl(memes_data[randomNumber].url);
      setRand((prevMeme) => ({
        ...prevMeme,
        randomImage: url,
      }));
    }
  }

  const [rand, setRand] = React.useState({
    topText: "",
    bottomText: "",
    randomImage: "https://i.imgflip.com/3si4.jpg",
  });

  function handleChange(event) {
    const { name, value } = event.target;
    setRand((prevMeme) => ({
      ...prevMeme,
      [name]: value,
    }));
  }
  return (
    <main>
      <div className="meme-card">
        <div className="input-buttons">
          <input
            type="enter first line"
            placeholder="Enter first line text here"
            className="input1"
            name="topText"
            value={rand.topText}
            onChange={handleChange}
          />
          <input
            type="enter next line"
            placeholder="Enter second line text here"
            className="input2"
            name="bottomText"
            value={rand.bottomText}
            onChange={handleChange}
          />
        </div>
        <button onClick={changeImage} className="input-img">
          Get a random Image
        </button>
      </div>
      <div className="meme-display">
        <div className="meme-text">
          <img
            src={rand.randomImage}
            alt="random-meme"
            className="meme-image"
          />
          <h2 className="meme-toptext">{rand.topText}</h2>
          <h2 className="meme-bottomtext">{rand.bottomText}</h2>
        </div>
      </div>
    </main>
  );
}

export { Card };
